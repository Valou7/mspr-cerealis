using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI; 

public class ApiCall : MonoBehaviour


{
    [SerializeField]
    private string uri = ".....uri...";// TODO
    [SerializeField]
    private Text textToControl;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PostRequest());
    }


    IEnumerator PostRequest()
    {
        WWWForm formData = new WWWForm(); //on cr�e l'objet de datas 

        formData.AddField("name", "loulou ");
        formData.AddField("email", "loulou@dot.fr ");

        using (UnityWebRequest request = UnityWebRequest.Post("uri.....", formData))
        {

            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
                textToControl.text = "result" + request.downloadHandler.text;
            }

        }
    }
}
